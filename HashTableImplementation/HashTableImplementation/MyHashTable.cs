﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableImplementation
{
    class MyHashTable   
    {
        HashNode element;

        private LinkedList<HashNode> hashTable { get; set; }

        public MyHashTable()
        {
             hashTable = new LinkedList<HashNode>();
        }

        private void KeyIsNull(object key)
        {
            if (key == null) throw new Exception("Key cannot be null");
        }

        private void AddValidation(HashNode element)
        {
            KeyIsNull(element.key);
            for (int i = 0; i <= hashTable.Count - 1; ++i)
            {
                HashNode current = hashTable.ElementAt(i);
                if (current.key.Equals(element.key))
                {
                    throw new ArgumentException("An element with the same key already exists in the Hashtable.");
                }
            }
        }

        public void Add(object key, object value)
        {
            element = new HashNode(key, value);

            try
            {
                AddValidation(element);
                if (hashTable.Count == 0)
                {
                    hashTable.AddFirst(element);
                }
                else hashTable.AddAfter(hashTable.Last, element);

                element = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Remove(object key)
        {
            try
            {
                KeyIsNull(key);
                for (int i = 0; i <= hashTable.Count - 1; ++i)
                {
                    HashNode current = hashTable.ElementAt(i);
                    if (current.key.Equals(key))
                    {
                        hashTable.Remove(current);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public bool ContainsKey(object key)
        {
            bool contains = false;
            try
            {
                KeyIsNull(key);
                for (int i = 0; i <= hashTable.Count - 1; ++i)
                {
                    HashNode current = hashTable.ElementAt(i);
                    if (current.key.Equals(key))
                    {
                        contains = true;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return contains;
        }

        public bool ContainsValue(object value)
        {
            bool contains = false;
            for (int i = 0; i <= hashTable.Count - 1; ++i)
            {
                HashNode current = hashTable.ElementAt(i);
                if (current.value.Equals(value))
                {
                    contains = true;
                    break;
                }
            }
            return contains;
        }

        public void Clear()
        {
            hashTable.Clear();
            element = null;
        }

        public void PrintKeysAndValues()
        {
            foreach (HashNode h in hashTable)
                Console.WriteLine("{0}\t\t{1}", h.key, h.value);
            Console.WriteLine();
        }

    }
}
