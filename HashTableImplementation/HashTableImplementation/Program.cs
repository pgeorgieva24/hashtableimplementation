﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;



namespace HashTableImplementation
{
    class Program
    {
        static void Main(string[] args)
        {
            MyHashTable p = new MyHashTable();

            p.Add("some", "work");
            p.Add(3, 21);
            p.Add(43, "haha");

            p.PrintKeysAndValues();

            p.Remove(3);

            p.PrintKeysAndValues();

            p.Add("add", "another element");

            Console.WriteLine(p.ContainsKey(43));
            Console.WriteLine(p.ContainsValue(43));
            
            p.Clear();

            p.PrintKeysAndValues();

            Console.ReadLine();
        }
    }
}
